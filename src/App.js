import React from 'react';
import { useState } from 'react';
import i18next from 'i18next'

import logo from './assets/Australian-Ethical.svg';
import './App.scss';
import {Next, Prev} from './components/Buttongraphy/button';
import Card from './components/Cardgraphy/card';

function App() {
  const [step, setStep] = useState(0);
  const [year, setYear] = useState('2022');
  const [human, setHuman] = useState(0);
  const [ethical, setEthical] = useState(0);
  const [social, setSocial] = useState(0);
  const [tech, setTech] = useState(0);
  const [economic, setEconomic] = useState(0);
  const [policy, setPolicy] = useState(0);
  const [humanLove, setHumanLove] = useState(false);
  const [ethicalLove, setEthicalLove] = useState(false);
  const [socialLove, setSocialLove] = useState(false);
  const [techLove, setTechLove] = useState(false);
  const [economicLove, setEconomicLove] = useState(false);
  const [policyLove, setPolicyLove] = useState(false);

  i18next.init({
    lng: 'en',
    debug: process.env.NODE_ENV === 'development',
    resources: {
        en: {
            translation: require('./assets/json/en.json'),
        },
    },
  });

  function generateCard(topic) {
    var item = i18next.t(`${year}.${topic}`, { returnObjects: true })
    var index = Math.floor(Math.random() * Math.floor(item.length))
    return index;
  }

  function validateLove() {
    var array = [humanLove, ethicalLove, socialLove, techLove, economicLove, policyLove];
    var count = 0;
    array.forEach(i => {
      if (i === true) {
        count += 1;
      }
    });

    if (count === 3) {
      window.scrollTo({top: 0, behavior: 'smooth'});
      return setStep(step+1)
    } else {
      return alert("Please select 3 ingredients")
    }
  }

  function cardMapper() {
    return (
      <>
        {humanLove === true ? (
          <Card
            topic="human"
            category="humans"
            title={i18next.t(`${year}.Humans.${human}.title`)}
            desc={i18next.t(`${year}.Humans.${human}.description`)}
            context={i18next.t(`${year}.Humans.${human}.context`)}
            loved={humanLove}
          />
        ) : null}
        {ethicalLove === true ? (
          <Card
            topic="ethical"
            category="ethical"
            title={i18next.t(`${year}.Ethical.${ethical}.title`)}
            desc={i18next.t(`${year}.Ethical.${ethical}.description`)}
            context={i18next.t(`${year}.Ethical.${ethical}.context`)}
            loved={ethicalLove}
          />
        ) : null}
        {socialLove === true ? (
          <Card
            topic="social"
            category="social &amp; cultural"
            title={i18next.t(`${year}.Social.${social}.title`)}
            desc={i18next.t(`${year}.Social.${social}.description`)}
            context={i18next.t(`${year}.Social.${social}.context`)}
            loved={socialLove}
          />
        ) : null}
        {techLove === true ? (
          <Card
            topic="technology"
            category="technology"
            title={i18next.t(`${year}.Technology.${tech}.title`)}
            desc={i18next.t(`${year}.Technology.${tech}.description`)}
            context={i18next.t(`${year}.Technology.${tech}.context`)}
            loved={techLove}
          />
        ) : null}
        {economicLove === true ? (
          <Card
            topic="economic"
            category="economic foundations"
            title={i18next.t(`${year}.Economic.${economic}.title`)}
            desc={i18next.t(`${year}.Economic.${economic}.description`)}
            context={i18next.t(`${year}.Economic.${economic}.context`)}
            loved={economicLove}
          />
        ) : null}
        {policyLove === true ? (
          <Card
            topic="policy"
            category="policy"
            title={i18next.t(`${year}.Policy.${policy}.title`)}
            desc={i18next.t(`${year}.Policy.${policy}.description`)}
            context={i18next.t(`${year}.Policy.${policy}.context`)}
            loved={policyLove}
          />
        ) : null}
      </>
    )
  }

  const steps = [
    <>
      <div className="intro-background">
        <div className="intro">
          <p className="part">{"/part 1"}</p>
          <h1 className="intro-primary-headline">CREATE YOUR</h1>
          <h1 className="intro-secondary-headline">WORLDVIEW</h1>
          {/* <p className="intro-description">A worldview is a hypothesis on what we suspect the world will look like, informed by human motivations, global trends and foreseeable disruptors of change. It is a view of what the world could be and how people might think, act, and behave as a result.</p> */}
        </div>
      </div>
    </>,
    <>
      <div className="header">
        <div className="header-title-wrapper">
          <p>Australian<br />Ethical</p>
        </div>
        <div className="header-logo-wrapper">
          <img className="header-logo-svg" src={logo} alt="Australian-Ethical" />
        </div>
      </div>
      <div className="year-picker-background">
        <div className="year-picker">
          <form id="nl-form" className="nl-form">
            EXPLORE A <span className="form-static-secondary-text">FUTURE</span> IN <span> </span>
            <select id="yearselect" onChange={e => setYear(e.target.value)}>
              <option value="2022">2022</option>
              <option value="2025">2025</option>
              <option value="2030">2030</option>
            </select>
          </form>
        </div>
      </div>
    </>,
    <>
      <div className="card-selection">
        <div className="headline-container">
          <div>
            YOUR WORLD&nbsp;<span className="form-static-secondary-text">INGREDIENTS</span>
          </div>
          <div className="description">
            <div className="desc-ico"></div>
            Pick 3 ingredients to construct your worldview.
          </div>
          {/* <div className="refresh-container" onClick={() => {
            setHuman(generateCard("Humans"));
            setEthical(generateCard("Ethical"));
            setSocial(generateCard("Social"));
            setTech(generateCard("Technology"));
            setEconomic(generateCard("Economic"));
            setPolicy(generateCard("Policy"));
            setHumanLove(false);
            setEthicalLove(false);
            setSocialLove(false);
            setTechLove(false);
            setEconomicLove(false);
            setPolicyLove(false);
          }}>
            <div className="refresh-ico"></div>
            REFRESH CARDS
          </div> */}
        </div>
        <div className="card-container">
          <Card
            topic="human"
            category="humans"
            title={i18next.t(`${year}.Humans.${human}.title`)}
            desc={i18next.t(`${year}.Humans.${human}.description`)}
            context={i18next.t(`${year}.Humans.${human}.context`)}
            onclick={() => {
              setHumanLove(!humanLove);
            }}
            loved={humanLove}
          />
          <Card
            topic="ethical"
            category="ethical"
            title={i18next.t(`${year}.Ethical.${ethical}.title`)}
            desc={i18next.t(`${year}.Ethical.${ethical}.description`)}
            context={i18next.t(`${year}.Ethical.${ethical}.context`)}
            onclick={() => {
              setEthicalLove(!ethicalLove)
            }}
            loved={ethicalLove}
          />
          <Card
            topic="social"
            category="social &amp; cultural"
            title={i18next.t(`${year}.Social.${social}.title`)}
            desc={i18next.t(`${year}.Social.${social}.description`)}
            context={i18next.t(`${year}.Social.${social}.context`)}
            onclick={() => {
              setSocialLove(!socialLove)
            }}
            loved={socialLove}
          />
          <Card
            topic="technology"
            category="technology"
            title={i18next.t(`${year}.Technology.${tech}.title`)}
            desc={i18next.t(`${year}.Technology.${tech}.description`)}
            context={i18next.t(`${year}.Technology.${tech}.context`)}
            onclick={() => {
              setTechLove(!techLove)
            }}
            loved={techLove}
          />
          <Card
            topic="economic"
            category="economic foundations"
            title={i18next.t(`${year}.Economic.${economic}.title`)}
            desc={i18next.t(`${year}.Economic.${economic}.description`)}
            context={i18next.t(`${year}.Economic.${economic}.context`)}
            onclick={() => {
              setEconomicLove(!economicLove)
            }}
            loved={economicLove}
          />
          <Card
            topic="policy"
            category="policy"
            title={i18next.t(`${year}.Policy.${policy}.title`)}
            desc={i18next.t(`${year}.Policy.${policy}.description`)}
            context={i18next.t(`${year}.Policy.${policy}.context`)}
            onclick={() => {
              setPolicyLove(!policyLove)
            }}
            loved={policyLove}
          />
        </div>
      </div>
    </>,
    <>
      <div className="card-selection">
        <div className="headline-container">
          <div>
            YOUR WORLD&nbsp;<span className="form-static-secondary-text">SELECTIONS</span>
          </div>
          <div className="description">
            <div className="desc-ico desc-ico-result"></div>
            You’re going to need to draw some rather large conclusions, make leaps and take creative license. It's not about getting the worldview 'right' or predicting the future, it's about imagining probable or possible new worlds - based on evidence - that allow us to generate new ideas!
          </div>
        </div>
        <div className="card-container">
          {cardMapper()}
        </div>
      </div>
    </>
  ]

  return (
    <div className="container">
      {steps[step]}
      {step === 0 ? (
        <div className="button-wrapper button-wrapper-single">
          <Next onclick={()=> setStep(step+1)}></Next>
        </div>
      ) : null}
      {step === 1 ? (
        <div className="button-wrapper">
          <Prev onclick={()=> setStep(step-1)}></Prev>
          <Next onclick={()=> {
              setStep(step+1);
              setHuman(generateCard("Humans"));
              setEthical(generateCard("Ethical"));
              setSocial(generateCard("Social"));
              setTech(generateCard("Technology"));
              setEconomic(generateCard("Economic"));
              setPolicy(generateCard("Policy"));
              setHumanLove(false);
              setEthicalLove(false);
              setSocialLove(false);
              setTechLove(false);
              setEconomicLove(false);
              setPolicyLove(false);
            }}></Next>
        </div>
      ) : null}
      {step === 2 ? (
        <div className="button-wrapper">
          <Prev onclick={()=> {
              setStep(step-1);
              setYear('2022');
            }}></Prev>
          <Next onclick={()=> validateLove()}></Next>
        </div>
      ) : null}
      {step === 3 ? (
        <div className="button-wrapper">
          <Prev onclick={()=> setStep(step-1)}></Prev>
        </div>
      ) : null}
    </div>
  );
}

export default App;
