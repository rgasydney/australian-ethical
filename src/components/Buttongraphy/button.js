import React from 'react';
import './button.scss';
import logo from '../../assets/Button-Logo.svg';
import borderLove from '../../assets/Border-Love-Logo.svg';
import fullLove from '../../assets/Full-Love-Logo.svg';
import moreLogo from '../../assets/More-Info-Logo.svg';

function Next({onclick}) {
  return (
    <div className="button-container" onClick={onclick}>
      <div className="button-text next">NEXT</div>
      <img className="logo rotated" src={logo} alt="next-button"></img>
    </div>
  );
}

function Prev({onclick}) {
  return (
    <div className="button-container" onClick={onclick}>
      <img className="logo" src={logo} alt="prev-button"></img>
      <div className="button-text prev">PREV</div>
    </div>
  )
}

function Love({onclick, loved}) {
  return (
    <div className="love-button-container" onClick={onclick}>
      <img className={`love-logo ${loved ? "hidden" : ""}`} src={borderLove} alt="love-button"></img>
      <img className={`love-logo ${loved ? "" : "hidden"}`} src={fullLove} alt="love-button"></img>
    </div>
  )
}

function More({onclick, flip}) {
  function mapper(flip) {
    if (flip === false) {
      return (
        <>
          <img className="more-logo" src={moreLogo} alt="more-info"></img>
          <p className="more-logo-text">More info</p>
        </>
      )
    } else {
      return(
        <>
          <img className="more-logo" src={logo} alt="more-info"></img>
          <p className="more-logo-text more-logo-text-back">Back</p>
        </>
      )
    }
  }
  return (
    <div className="more-button-container" onClick={onclick}>
      {mapper(flip)}
    </div>
  )
}

export { Prev, Next, Love, More };