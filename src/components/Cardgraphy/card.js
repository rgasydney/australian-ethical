import React from 'react';
import { useState } from 'react';

import './card.scss';
import { Love, More } from '../Buttongraphy/button';

function Card({key, topic, category, title, desc, context, onclick, loved}) {
  const [flip,setFlip] = useState(false);
  const splitContext = context.split("<br/>");
  const splitDesc = desc.split("<br/>");

  return (
    <div key={key}>
      <div className="love-button-wrapper">
        <Love onclick={onclick} loved={loved}/>
      </div>
      <div className={`card ${topic}`}>
        <div className={`card-wrapper ${flip ? "flipped" : ""}`}>
          <p className="card-category">{category}</p>
          <h1 className="card-title">{title}</h1>
          {splitDesc.map((item, index) => (<p className="card-desc" key={index}>{item}</p>))}
        </div>
        <div className={`card-wrapper ${flip ? "" : "flipped"}`}>
          <p className="card-category card-category-flipped">{category}</p>
          <h1 className="card-title card-title-flipped">{title}</h1>
          {splitContext.map((item, index) => (<p className="card-desc card-desc-flipped" key={index}>{item}</p>))}
        </div>
      </div>
      <div className="more-button-wrapper">
        <More onclick={()=> setFlip(!flip)} flip={flip}/>
      </div>
    </div>
  );
}

export default Card;